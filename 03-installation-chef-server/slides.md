%title: CHEF
%author: xavki



 ██████╗██╗  ██╗███████╗███████╗    ██╗███╗   ██╗███████╗██████╗  █████╗ 
██╔════╝██║  ██║██╔════╝██╔════╝    ██║████╗  ██║██╔════╝██╔══██╗██╔══██╗
██║     ███████║█████╗  █████╗      ██║██╔██╗ ██║█████╗  ██████╔╝███████║
██║     ██╔══██║██╔══╝  ██╔══╝      ██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║
╚██████╗██║  ██║███████╗██║         ██║██║ ╚████║██║     ██║  ██║██║  ██║
 ╚═════╝╚═╝  ╚═╝╚══════╝╚═╝         ╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝


-----------------------------------------------------------------------------------------------------------                                          

# CHEF : Installation de Chef Server


Liens à connaître :

* https://docs.chef.io/server/install_server/
* https://www.chef.io/downloads/tools/infra-server


sudo apt-get update -y -qq > /dev/null
sudo apt-get upgrade -y -qq > /dev/null

wget -qq -P /tmp https://packages.chef.io/files/stable/chef-server/15.6.2/ubuntu/20.04/chef-server-core_15.6.2-1_amd64.deb > /dev/null
dpkg -i /tmp/chef-server-core_15.6.2-1_amd64.deb

mkdir /home/vagrant/certs


sudo chef-server-ctl reconfigure --chef-license=accept

sudo chef-server-ctl service-list

	* oc_bifrost : authentification

	* bookshelf : stockage des cookbooks

	* opscode-erchef : api chef

	*	Interaction : chef-server-ctl restart bifrost

	* chef-server-ctl install chef-manage

sudo chef-server-ctl user-create xavki xav ki xavki@moi.fr 'password' --filename /home/vagrant/certs/xavki.pem
sudo chef-server-ctl org-create xavorg "Test xavki" --association_user xavki --filename /home/vagrant/certs/xavorg.pem

Préparation accès ssh :

ssh-keygen -t ed25519 -a 64
sudo mkdir -p /root/.ssh
sudo cp .ssh/id_ed25519.pub /root/.ssh/authorized_keys

Test :
ssh -i id_ed25519 root@192.168.144.51
