%title: CHEF
%author: xavki



 ██████╗██╗  ██╗███████╗███████╗    ██╗███╗   ██╗███████╗██████╗  █████╗ 
██╔════╝██║  ██║██╔════╝██╔════╝    ██║████╗  ██║██╔════╝██╔══██╗██╔══██╗
██║     ███████║█████╗  █████╗      ██║██╔██╗ ██║█████╗  ██████╔╝███████║
██║     ██╔══██║██╔══╝  ██╔══╝      ██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║
╚██████╗██║  ██║███████╗██║         ██║██║ ╚████║██║     ██║  ██║██║  ██║
 ╚═════╝╚═╝  ╚═╝╚══════╝╚═╝         ╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝


-----------------------------------------------------------------------------------------------------------                                          

# CHEF : Installation de Chef Workstation


Liens à connaître :

* https://www.chef.io/downloads/tools/workstation

sudo apt-get update -y -qq > /dev/null
sudo apt-get upgrade -y -qq > /dev/null

wget -qq -P /tmp https://packages.chef.io/files/stable/chef-workstation/22.1.745/ubuntu/20.04/chef-workstation_22.1.745-1_amd64.deb

sudo dpkg -i /tmp/chef-workstation_22.1.745-1_amd64.deb

chef generate repo chef-repo --chef-license=accept

mkdir -p /home/vagrant/chef-repo/.chef

mkdir -p /home/vagrant/.ssh/
echo "
Host *
  StrictHostKeyChecking no
"> /home/vagrant/.ssh/config

```
scp -i /vagrant/id_ed25519 root@chefserver:/home/vagrant/certs/*.pem /home/vagrant/chef-repo/.chef/
```
cd /home/vagrant/chef-repo/

git config --global user.email "you@example.com"
git config --global user.name "xavki"

cp /vagrant/config.rb /home/vagrant/chef-repo/.chef/config.rb

knife ssl fetch
knife client list


