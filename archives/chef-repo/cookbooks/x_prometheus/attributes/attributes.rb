default['x_prometheus']['version'] = "2.44.0"
default['x_prometheus']['dir'] = "/opt/prometheus"
default['x_prometheus']['user'] = "prometheus"
default['x_prometheus']['group'] = default['x_prometheus']['user'] 
default['x_prometheus']['dir_log'] = "/var/log/prometheus"
default['x_prometheus']['dir_data'] = "/var/lib/prometheus"
default['x_prometheus']['dir_conf'] = "/etc/prometheus"
default['x_prometheus']['checksum'] = "be5c8e43618999c3109c1416e04e4ce25c689f82388db6d275a245fe5b1daae7"
