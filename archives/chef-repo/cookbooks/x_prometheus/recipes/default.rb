#
# Cookbook:: prometheus
# Recipe:: default
#
# Copyright:: 2023, The Authors, All Rights Reserved.
#

version = node['x_prometheus']['version']
node_exporter_srv = search(:node, "role:node_exporter")

user node['x_prometheus']['user'] do
  system true
  shell '/bin/false'
  home node['x_prometheus']['dir']
end

directory node['x_prometheus']['dir_log'] do
  owner node['x_prometheus']['user']
  group node['x_prometheus']['group']
  mode '0755'
  recursive true
end

directory node['x_prometheus']['dir_data'] do
  owner node['x_prometheus']['user']
  group node['x_prometheus']['group']
  mode '0755'
  recursive true
end

directory node['x_prometheus']['dir_conf'] do
  owner node['x_prometheus']['user']
  group node['x_prometheus']['group']
  mode '0755'
  recursive true
end

remote_file "/opt/prometheus-#{version}.linux-amd64.tar.gz" do
  source "https://github.com/prometheus/prometheus/releases/download/v#{version}/prometheus-#{version}.linux-amd64.tar.gz"
  owner node['x_prometheus']['user']
  group node['x_prometheus']['group']
  mode '0755'
  checksum node['x_prometheus']['checksum']
  notifies :extract, "archive_file[prometheus-#{version}.linux-amd64.tar.gz]", :immediately
  not_if { ::File.exists?("/opt/prometheus-#{version}.linux-amd64.tar.gz")}
end

archive_file "prometheus-#{version}.linux-amd64.tar.gz" do
  owner node['x_prometheus']['user']
  group node['x_prometheus']['group']
  mode '755'
  path "/opt/prometheus-#{version}.linux-amd64.tar.gz"
  destination "/opt/prometheus/"
  notifies :create, 'remote_file[Copy prometheus binary]', :immediately
  notifies :create, 'remote_file[Copy promtool binary]', :immediately
  notifies :run, 'bash[Move consoles directory]', :immediately
  notifies :run, 'bash[Move consoles libraries directory]', :immediately
  action :nothing
end

remote_file "Copy prometheus binary" do
  path "/usr/local/bin/prometheus"
  source "file:///opt/prometheus/prometheus-#{version}.linux-amd64/prometheus"
  owner node['x_prometheus']['user']
  group node['x_prometheus']['group']
  mode 0755
  action :nothing
end

remote_file "Copy promtool binary" do
  path "/usr/local/bin/promtool"
  source "file:///opt/prometheus/prometheus-#{version}.linux-amd64/promtool"
  owner node['x_prometheus']['user']
  group node['x_prometheus']['group']
  mode 0755
  action :nothing
end


bash "Move consoles directory" do
  code <<-EOH
    [[ -e "/opt/prometheus/prometheus-#{version}.linux-amd64/consoles/" ]] && cp -r /opt/prometheus/prometheus-#{version}.linux-amd64/consoles/ /etc/prometheus/
    chown #{node['x_prometheus']['user']}:#{node['x_prometheus']['group']} /etc/prometheus/consoles
  EOH
  action :nothing
end

bash "Move consoles libraries directory" do
  code <<-EOH
    [[ -e "/opt/prometheus/prometheus-#{version}.linux-amd64/consoles/" ]] && cp -r /opt/prometheus/prometheus-#{version}.linux-amd64/console_libraries/ /etc/prometheus/
    chown #{node['x_prometheus']['user']}:#{node['x_prometheus']['group']} /etc/prometheus/console_libraries
  EOH
  action :nothing
end

template 'Prometheus configuration' do
  path "#{node['x_prometheus']['dir_conf']}/prometheus.yml"
  source 'prometheus.yml.erb'
  owner node['x_prometheus']['user']
  group node['x_prometheus']['group']
  mode '0755'
  variables( :node_exporter_srv => node_exporter_srv )
  notifies :restart, 'systemd_unit[prometheus_action]', :delayed
end

systemd_unit 'prometheus.service' do
  content <<~EOU
  [Unit]
  Description=Prometheus Server #{version}
  Wants=network-online.target
  After=network-online.target
  
  [Service]
  Type=simple
  User=#{node['x_prometheus']['user']}
  Group=#{node['x_prometheus']['group']}
  ExecStart=/usr/local/bin/prometheus \
  --config.file /etc/prometheus/prometheus.yml \
  --storage.tsdb.path /var/lib/prometheus/ \
  --web.console.templates=/etc/prometheus/consoles \
  --web.console.libraries=/etc/prometheus/console_libraries
  
  [Install]
  WantedBy=multi-user.target
  EOU

  notifies :restart, 'systemd_unit[prometheus_action]', :delayed
  action [:create, :enable, :start]
end

systemd_unit 'prometheus_action' do
  unit_name 'prometheus.service'
  action [:nothing]
end
