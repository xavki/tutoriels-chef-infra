#
# Cookbook:: grafana
# Recipe:: default
#
# Copyright:: 2023, The Authors, All Rights Reserved.
#
#


apt_repository 'grafana' do
  uri          'https://packages.grafana.com/oss/deb'
  arch         'amd64'
  distribution 'stable'
  components   ['main']
  key          'https://packages.grafana.com/gpg.key'
end

apt_package 'grafana' do
  version node['x_grafana']['version']
  notifies :restart, 'systemd_unit[grafana-server]', :delayed
  notifies :run, 'execute[grafana-admin-password]', :delayed
end

execute 'grafana-admin-password' do
  command "grafana-cli admin reset-admin-password #{node['x_grafana']['admin-password']}"
  action :nothing
end

template 'Add prometheus to grafana' do
  path "#{node['x_grafana']['dir_data']}/datasources/prometheus.yaml"
  source 'prometheus.yaml.erb'
  owner 'root'
  group 'grafana'
  mode '0755'
  notifies :restart, 'systemd_unit[grafana-server]', :delayed
end

remote_file 'node_exporter_dashboard' do
  source 'https://raw.githubusercontent.com/rfrail3/grafana-dashboards/master/prometheus/node-exporter-full.json'
  path '/var/lib/grafana/node-exporter.json'
  owner 'root'
  group 'grafana'
  mode '0755'
  notifies :restart, 'systemd_unit[grafana-server]', :delayed
end

template 'deploy_node_exporter_dashboard' do
  path "#{node['x_grafana']['dir_data']}/dashboards/node-exporter.yaml"
  source 'node-exporter.yaml.erb'
  owner 'root'
  group 'grafana'
  mode '0755'
  notifies :restart, 'systemd_unit[grafana-server]', :delayed
end

systemd_unit 'grafana-server' do
  action :start
end
