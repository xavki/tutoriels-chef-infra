default['x_grafana']['version'] = "10.0.1"
default['x_grafana']['admin-password'] = "password"
default['x_grafana']['prometheus_url'] = "127.0.0.1"
default['x_grafana']['dir_data'] = "/etc/grafana/provisioning"
