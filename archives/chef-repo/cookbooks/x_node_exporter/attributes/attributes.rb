default['x_node_exporter']['version'] = "1.6.0"
default['x_node_exporter']['dir'] = "/opt/node_exporter"
default['x_node_exporter']['user'] = "node_exporter"
default['x_node_exporter']['group'] = default['x_node_exporter']['user'] 
default['x_node_exporter']['checksum'] = "0b3573f8a7cb5b5f587df68eb28c3eb7c463f57d4b93e62c7586cb6dc481e515"
