#
# Cookbook:: node_exporter
# Recipe:: default
#
# Copyright:: 2023, The Authors, All Rights Reserved.
#

version = node['x_node_exporter']['version']

user node['x_node_exporter']['user'] do
  system true
  shell '/bin/false'
  home node['x_node_exporter']['dir']
end

directory node['x_node_exporter']['dir'] do
  owner node['x_node_exporter']['user']
  group node['x_node_exporter']['group']
  mode '0755'
  recursive true
end

remote_file "/opt/node_exporter-#{version}.linux-amd64.tar.gz" do
  source "https://github.com/prometheus/node_exporter/releases/download/v#{version}/node_exporter-#{version}.linux-amd64.tar.gz"
  owner node['x_node_exporter']['user']
  group node['x_node_exporter']['group']
  mode '0755'
  checksum node['x_node_exporter']['checksum']
  notifies :extract, "archive_file[node_exporter-unarchive]", :immediately
  not_if { ::File.exists?("/opt/node_exporter-#{version}.linux-amd64.tar.gz")}
end

archive_file "node_exporter-unarchive" do
  owner node['x_node_exporter']['user']
  group node['x_node_exporter']['group']
  mode '755'
  path "/opt/node_exporter-#{version}.linux-amd64.tar.gz"
  destination "/opt/node_exporter/node_exporter-#{version}.linux-amd64/"
  notifies :create, 'remote_file[Copy node_exporter binary]', :immediately
  action :nothing
end

remote_file "Copy node_exporter binary" do
  path "/usr/local/bin/node_exporter"
  source "file:///opt/node_exporter/node_exporter-#{version}.linux-amd64/node_exporter-#{version}.linux-amd64/node_exporter"
  owner node['x_node_exporter']['user']
  group node['x_node_exporter']['group']
  mode 0755
  action :nothing

  notifies :restart, 'systemd_unit[node_exporter_restart]', :delayed
end

systemd_unit 'node_exporter.service' do
  content <<~EOU
  [Unit]
  Description=Node Exporter - #{version}
  Wants=network-online.target
  After=network-online.target

  [Service]
  Type=simple
  User=#{node['x_node_exporter']['user']}
  Group=#{node['x_node_exporter']['group']}
  ExecStart=/usr/local/bin/node_exporter

  [Install]
  WantedBy=multi-user.target
  EOU
  action [:create, :enable, :start]

  notifies :restart, 'systemd_unit[node_exporter_restart]', :delayed
end

systemd_unit 'node_exporter_restart' do
  unit_name 'node_exporter.service'
  action [:nothing]
end
