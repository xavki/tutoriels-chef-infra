%title: CHEF
%author: xavki



 ██████╗██╗  ██╗███████╗███████╗    ██╗███╗   ██╗███████╗██████╗  █████╗ 
██╔════╝██║  ██║██╔════╝██╔════╝    ██║████╗  ██║██╔════╝██╔══██╗██╔══██╗
██║     ███████║█████╗  █████╗      ██║██╔██╗ ██║█████╗  ██████╔╝███████║
██║     ██╔══██║██╔══╝  ██╔══╝      ██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║
╚██████╗██║  ██║███████╗██║         ██║██║ ╚████║██║     ██║  ██║██║  ██║
 ╚═════╝╚═╝  ╚═╝╚══════╝╚═╝         ╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝


-----------------------------------------------------------------------------------------------------------                                          

# CHEF : Définition & Concepts


Client

	* serveur cible à configurer

	* passer d'un état à un état décrit

	* ce sont les nodes

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : Définition & Concepts


Server

	* serveur central Chef

	* base de données : attributs, cookbooks, roles...

	* source pour les agents (clients)

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : Définition & Concepts


Workstation

	* machine de développement (server, laptop...)

	* commandes knife, chef...

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : Définition & Concepts


Cookbooks

	* boite composée de recettes (recipes) 

	* objectif logique global (monitoring)

	* composés : recipes, tempaltes, attributes, resources, files...

	* découper en recette (fichiers recipes)

	* poussé/versionné sur chef-server (pas directement les recipes)

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : Définition & Concepts


Recipes

	* la description des actions techniques

	* sous forme de fichier

	* ne contient pas les attributes (cookbooks)

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : Définition & Concepts


Attributes

	* équivalent de variable

	* générique ou spécifiques

	* définissable à différents niveau (recipes, rôles, nodes)

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : Définition & Concepts


Resources

	* tâches ou actions individuelles

	* natives ou personnalisées (répertoire resources d'un CB)

```
file '/path/to/myfile.txt' do
  owner 'root'
  group 'root'
  mode '0644'
  content 'Ceci est le contenu du fichier.'
  action :create
end
```

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : Définition & Concepts


Templates

	* fichier avec extension erb

	* intégrant des attributes (variables)

	* avec du code ruby si nécessaire (boucles...)

Files

	* fichier sans attributes

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : Définition & Concepts


Databags

	* autre moyen pour entreposer des variables

	* format JSON

	* "variables variables" (environnement...)

	* variables sécurisées

	* structure customisable sous forme d'arborescence

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : Définition & Concepts


Cookbook > recipes + attributes + templates + files + resources

Recipe > fichiers tâches (resources)
