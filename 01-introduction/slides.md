%title: CHEF
%author: xavki



 ██████╗██╗  ██╗███████╗███████╗    ██╗███╗   ██╗███████╗██████╗  █████╗ 
██╔════╝██║  ██║██╔════╝██╔════╝    ██║████╗  ██║██╔════╝██╔══██╗██╔══██╗
██║     ███████║█████╗  █████╗      ██║██╔██╗ ██║█████╗  ██████╔╝███████║
██║     ██╔══██║██╔══╝  ██╔══╝      ██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║
╚██████╗██║  ██║███████╗██║         ██║██║ ╚████║██║     ██║  ██║██║  ██║
 ╚═════╝╚═╝  ╚═╝╚══════╝╚═╝         ╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝


-----------------------------------------------------------------------------------------------------------                                          

# CHEF : introduction - c'est quoi ??


* Opensource

* Créé en 2009 - Adam Jacob

* Chef = chef Infra

* développé par Chef Software

* automatisation d'infrastructures - déclaratif

* compatible sur de nombreux OS :
		Ubuntu, Redhat, Windows, MacOS...

* mode client / serveur (agent)

* langage Ruby (comme puppet) & template erb

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : introduction - c'est quoi ??


Utilisé par :

	* Facebook : https://engineering.fb.com/2016/04/15/core-data/facebook-chef-cookbooks/

	* Bloomberg : https://github.com/bloomberg/chef-bcpc

	* IBM

	* Gitlab

Sites

	* officiel : https://www.chef.io/products/chef-infra

	* github : https://github.com/chef/chef

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : introduction - c'est quoi ??


* concurrents :

		* ansible

		* saltstack

		* puppet...

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : introduction - c'est quoi ??


* fonctionnalités : 

		* gestion des configurations

		* automatisation des tâches (centralisées)

		* orchestration (actions coordonnées)

		* surveillance et remédiation

		* prise en compte multi-environnement

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : introduction - c'est quoi ??


* Termes basés sur le domaine de la Cuisine :

		* Recipes

		* Cookbooks

		* Databags

		* Bookshelf

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : introduction - c'est quoi ??


Outils :

		* Chef Infra

		* Chef Habitat : déploiement applications	

		* Chef InSpec : tests

		* Chef Automate 

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : introduction - c'est quoi ??


Chef Workstation

	* environnement de développement

	* création de cookbooks

	* ligne de commande

	* permet de pousser vers chef server

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : introduction - c'est quoi ??


Chef Client

	* agent

	* déploiement de recettes

	* venant du chef serveur

	* applicaiton de configuration

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : introduction - c'est quoi ??


Chef Server

	* stockage des configurations

	* des données collectées

	* assure la connexion sécurisée avec les agents

	* édition des configurations

	* outils : Ruby/Rails / Postgresql / nginx / erlang / redis / java/opensearch

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : introduction - c'est quoi ??


Chef Supermarket

	* site web

	* partage de recettes

	* ansible galaxy like

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : introduction - c'est quoi ??


Chef Inspec

	* framework de tests pour chef

	* audits/tests des applications

	* comparaison du descriptif vs fonctionnel

	* rapport de résultat

	* Ruby dsl

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : introduction - c'est quoi ??


Chef SDK

	* outils locaux

	* développements

	* tests

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : introduction - c'est quoi ??


Chef Automate

	* Chef infra complété

	* outil pour veiller à la consitence

	* rapports d'état

	* gestion des workflows

	* maintien de sécurité

-----------------------------------------------------------------------------------------------------------                                          

# CHEF : introduction - c'est quoi ??


Knife

	* outil CLI

	* gestion à distance du chef serveur

	* ajout de recettes

	* edition de nodes



